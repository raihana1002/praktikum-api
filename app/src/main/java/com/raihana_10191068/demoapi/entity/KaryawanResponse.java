package com.raihana_10191068.demoapi.entity;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class KaryawanResponse{

	@SerializedName("result")
	private List<KaryawanItem> result;

	@SerializedName("status")
	private String status;

	public List<KaryawanItem> getResult(){
		return result;
	}

	public String getStatus(){
		return status;
	}
}