package com.raihana_10191068.demoapi.entity;

import com.google.gson.annotations.SerializedName;

public class KaryawanItem {

	@SerializedName("umur")
	private String umur;

	@SerializedName("nama")
	private String nama;

	@SerializedName("id_karyawan")
	private String idKaryawan;

	public String getUmur(){
		return umur;
	}

	public String getNama(){
		return nama;
	}

	public String getIdKaryawan(){
		return idKaryawan;
	}
}