package com.raihana_10191068.demoapi.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.widget.ProgressBar;

import com.raihana_10191068.demoapi.R;

public class MainActivity extends AppCompatActivity {
    RecyclerView rvMain;
    ProgressBar pbMain;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        rvMain = findViewById(R.id.rvMain);
        pbMain = findViewById(R.id.pbMain);
    }
}