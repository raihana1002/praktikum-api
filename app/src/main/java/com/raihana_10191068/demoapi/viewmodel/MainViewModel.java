package com.raihana_10191068.demoapi.viewmodel;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.raihana_10191068.demoapi.entity.KaryawanItem;
import com.raihana_10191068.demoapi.entity.KaryawanResponse;
import com.raihana_10191068.demoapi.util.ApiConfig;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainViewModel extends ViewModel {
    public MainViewModel(){

    }

    private final MutableLiveData<Boolean> _isloading = new MutableLiveData<>();
    public LiveData<Boolean> isloading(){
        return _isloading;
    }

    private final MutableLiveData<List<KaryawanItem>> _getAllKaryawan = new MutableLiveData<>();
    public LiveData<List<KaryawanItem>> getKaryawan() {
        return _getAllKaryawan;
    }

    public final void getAllKaryawan() {
        _isloading.setValue(true);
        Call<KaryawanResponse> client = ApiConfig.getApiService().getAllKaryawan();
        client.enqueue(new Callback<KaryawanResponse>() {
            @Override
            public void onResponse(Call<KaryawanResponse> call, Response<KaryawanResponse> response) {
                _isloading.setValue(false);
                if (response.isSuccessful()) {
                    if (response.body() != null){
                        _getAllKaryawan.setValue(response.body().getResult());
                    }
                }else {
                    if (response.body() != null) {
                        Log.e("ErrorMainViewModel", response.body().getStatus());
                    }
                }
            }

            @Override
            public void onFailure(Call<KaryawanResponse> call, Throwable t) {
                _isloading.setValue(false);
                Log.e("ErrorMainViewModel", t.getMessage());

            }
        });
    }
}
