package com.raihana_10191068.demoapi.util;

import com.raihana_10191068.demoapi.entity.KaryawanResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;

public interface ApiService {
    @Headers({
            "Client-Service : contoh-client",
            "Auth-Key : contohapi"
    })

    @GET("karyawan/get_all")
    Call<KaryawanResponse> getAllKaryawan();
}

