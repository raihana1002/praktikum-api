package com.raihana_10191068.demoapi.adapter;

import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.raihana_10191068.demoapi.R;
import com.raihana_10191068.demoapi.entity.KaryawanItem;

import java.util.List;

public class KaryawanAdapter extends RecyclerView.Adapter<KaryawanAdapter.KaryawanViewHolder> {
    private List<KaryawanItem> list;

    public KaryawanAdapter(List<KaryawanItem> list) {
        this.list = list;
    }

    public void insert(List<KaryawanItem> list) {
        this.list.clear();
        this.list.addAll(list);

        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public KaryawanViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull KaryawanViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 0;
    }

    public class KaryawanViewHolder extends RecyclerView.ViewHolder {
        private final TextView txtNama, txtUmur;

        public KaryawanViewHolder(@NonNull View itemView) {
            super (itemView);

            txtNama = itemView.findViewById(R.id.txtKaryawanItemName);
            txtUmur = itemView.findViewById(R.id.txtKaryawanItemAge);
        }
    }
}
